#!/bin/sh
unset RUN_DOCKERIZED

while getopts 'd' opt
do
  case $opt in
    d) RUN_DOCKERIZED=true;;
    *) echo "Error: Invalid flag";;
  esac
done

if [ "$RUN_DOCKERIZED" = true ]
then
  docker-compose up
else
  . ./.env
  export FAST_REFRESH=false

  npm run start
fi
