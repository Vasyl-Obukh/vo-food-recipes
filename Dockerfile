# STAGE 1: Build
FROM node:current-alpine AS build
ARG REACT_APP_API_SECRET
ENV REACT_APP_API_SECRET=$REACT_APP_API_SECRET
WORKDIR /build
COPY . .
RUN npm ci
RUN npm run build

# STAGE 2: Run
FROM node:current-alpine
WORKDIR /app
COPY --from=build /build/build /build/server.js ./
RUN npm install express http-proxy-middleware
CMD node server.js
