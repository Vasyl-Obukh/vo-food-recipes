interface CreateActionTypeFunction {
  (actionType: string): string;
}

export const createStoreActionType = (
  storeName: string
): CreateActionTypeFunction => (actionType: string): string =>
  `${storeName}/${actionType}`;

export const withPayloadType = <T>() => (payload: T) => ({ payload });
