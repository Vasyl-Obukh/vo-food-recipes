import { RootReducer } from 'store/types';

import { ExtendedRecipe, Slice } from './types';

export const selectSlice = (state: RootReducer): Slice => state.recipe || {};
export const selectRecipe = (state: RootReducer): ExtendedRecipe | null =>
  selectSlice(state).data || null;
export const selectRecipeLoadingState = (state: RootReducer): boolean =>
  selectSlice(state).loading;
