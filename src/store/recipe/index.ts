import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { Slice, ExtendedRecipe } from './types';
import * as sagaActions from './actions';

const recipesSlice = createSlice({
  name: 'recipe',
  initialState: {
    data: null,
    loading: false,
  } as Slice,
  reducers: {
    setRecipe(state, action: PayloadAction<ExtendedRecipe>) {
      state.data = action.payload;
    },
    clearRecipe(state) {
      state.data = null;
    },
    setLoadingState(state, action: PayloadAction<boolean>) {
      state.loading = action.payload;
    },
  },
});

export const { fetchRecipe, setRecipe, clearRecipe, setLoadingState } = {
  ...recipesSlice.actions,
  ...sagaActions,
};

export default recipesSlice.reducer;
