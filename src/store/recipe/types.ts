import { Recipe } from 'store/recipes/types';
import { Cuisine } from 'types/cuisine';
import { Diet } from 'types/diet';

export interface ExtendedRecipe extends Recipe {
  vegetarian: boolean;
  vegan: boolean;
  glutenFree: boolean;
  cheap: boolean;
  pricePerServing: number;
  readyInMinutes: number;
  cuisines: Cuisine[];
  summary: string;
  diets: Diet[];
}

export interface Slice {
  data: ExtendedRecipe | null;
  loading: boolean;
}
