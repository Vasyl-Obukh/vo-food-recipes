import { PayloadAction } from '@reduxjs/toolkit';
import { call, put, takeLatest } from 'redux-saga/effects';

import * as globalActions from 'store/global';
import { MessageType } from 'store/global/types';

import * as actions from '.';
import * as sagaActions from './actions';
import * as Api from './api';
import { ExtendedRecipe } from './types';
import { formatRecipeResponse } from './utils';

function* fetchRecipe({ payload: id }: PayloadAction<number>) {
  try {
    yield put(actions.setLoadingState(true));

    const response = yield call(Api.fetchRecipe, id);
    const recipe: ExtendedRecipe = yield call(formatRecipeResponse, response);

    yield put(actions.setRecipe(recipe));
  } catch (error) {
    yield put(
      globalActions.addMessage({
        type: MessageType.Error,
        text: error.message,
      })
    );
  } finally {
    yield put(actions.setLoadingState(false));
  }
}

export function* watchRecipeFetch() {
  yield takeLatest(sagaActions.fetchRecipe.toString(), fetchRecipe);
}

export default [watchRecipeFetch()];
