import { createAction } from '@reduxjs/toolkit';

import { createStoreActionType, withPayloadType } from 'store/utils/actionType';

const createRecipeActionType = createStoreActionType('recipe');

export const fetchRecipe = createAction(
  createRecipeActionType('recipeFetch'),
  withPayloadType<number>()
);
