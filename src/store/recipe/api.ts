import { HttpClient } from 'utils/httpClient';
import { API_ROOT } from 'configs/apiRoot';

export const fetchRecipe = async (id: number) =>
  HttpClient.get(`${API_ROOT}/${id}/information`);
