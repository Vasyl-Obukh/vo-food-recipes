import { ExtendedRecipe } from './types';

export const formatRecipeResponse = ({
  id,
  title,
  image,
  vegetarian,
  vegan,
  glutenFree,
  cheap,
  pricePerServing,
  readyInMinutes,
  cuisines,
  summary,
  diets,
}: ExtendedRecipe): ExtendedRecipe => ({
  id,
  title,
  image,
  vegetarian,
  vegan,
  glutenFree,
  cheap,
  pricePerServing,
  readyInMinutes,
  cuisines,
  summary,
  diets,
});
