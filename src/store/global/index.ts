import { createSlice, nanoid, PayloadAction } from '@reduxjs/toolkit';

import { Message, MessageType, Slice } from './types';
import * as sagaActions from './actions';

const globalSlice = createSlice({
  name: 'global',
  initialState: {
    messages: [],
  } as Slice,
  reducers: {
    setMessage: {
      reducer(state, action: PayloadAction<Message>) {
        state.messages.push(action.payload);
      },
      prepare(type: MessageType, text: string) {
        return {
          payload: {
            id: nanoid(),
            type,
            text,
          },
        };
      },
    },
    deleteMessageById(state, action: PayloadAction<string>) {
      state.messages = state.messages.filter(({ id }) => id !== action.payload);
    },
  },
});

export const { addMessage, setMessage, deleteMessageById } = {
  ...globalSlice.actions,
  ...sagaActions,
};

export default globalSlice.reducer;
