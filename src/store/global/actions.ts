import { createAction } from '@reduxjs/toolkit';

import { createStoreActionType, withPayloadType } from 'store/utils/actionType';
import { MessageType } from './types';

export interface AddMessagePayloadType {
  type: MessageType;
  text: string | string[];
}

const createGlobalActionType = createStoreActionType('global');

export const addMessage = createAction(
  createGlobalActionType('messageAdd'),
  withPayloadType<AddMessagePayloadType>()
);
