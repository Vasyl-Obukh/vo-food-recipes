import { all, put, takeLatest } from 'redux-saga/effects';
import { PayloadAction } from '@reduxjs/toolkit';

import * as actions from '.';
import { AddMessagePayloadType } from './actions';

function* addMessage({
  payload: { type, text },
}: PayloadAction<AddMessagePayloadType>) {
  const messages: string[] = Array.isArray(text) ? text : [text];
  yield all(messages.map((message) => put(actions.setMessage(type, message))));
}

export function* watchMessageAdd() {
  yield takeLatest(actions.addMessage.toString(), addMessage);
}

export default [watchMessageAdd()];
