import { RootReducer } from 'store/types';

import { Message, Slice } from './types';

export const selectSlice = (state: RootReducer): Slice => state.global || {};
export const selectMessages = (state: RootReducer): Message[] =>
  selectSlice(state).messages || [];
