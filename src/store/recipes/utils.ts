import { RecipesAttributes } from './types';

export const formatRecipesResponse = ({
  results,
  offset,
  totalResults,
}: RecipesAttributes): RecipesAttributes => ({
  results,
  offset,
  totalResults,
});
