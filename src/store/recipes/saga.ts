import { PayloadAction } from '@reduxjs/toolkit';
import { call, put, select, takeLatest } from 'redux-saga/effects';

import * as globalActions from 'store/global';
import { MessageType } from 'store/global/types';

import * as actions from '.';
import * as sagaActions from './actions';
import * as Api from './api';
import { formatRecipesResponse } from './utils';
import { AMOUNT_PER_FETCH } from './constants';
import { FiltersFormData, RecipesAttributes } from './types';
import { selectRecipesOffset, selectRecipesTotalResults } from './selectors';

function* fetchInitialRecipes({
  payload: params,
}: PayloadAction<FiltersFormData>) {
  try {
    yield put(actions.clearRecipes());
    yield put(actions.setLoadingState(true));

    const response = yield call(Api.fetchRecipes, {
      ...params,
      number: AMOUNT_PER_FETCH,
    });
    const recipesAttributes: RecipesAttributes = yield call(
      formatRecipesResponse,
      response
    );

    yield put(actions.updateRecipes(recipesAttributes));
  } catch (error) {
    yield put(
      globalActions.addMessage({
        type: MessageType.Error,
        text: error.message,
      })
    );
  } finally {
    yield put(actions.setLoadingState(false));
  }
}

function* checkIfThereIsMoreResults() {
  const currentOffset = yield select(selectRecipesOffset);
  const nextOffset = currentOffset + AMOUNT_PER_FETCH;
  const totalResults = yield select(selectRecipesTotalResults);

  return nextOffset < totalResults;
}

function* fetchMoreRecipes({
  payload: params,
}: PayloadAction<FiltersFormData>) {
  const isThereMoreResults = yield call(checkIfThereIsMoreResults);
  if (!isThereMoreResults) {
    return;
  }

  try {
    const currentOffset = yield select(selectRecipesOffset);

    yield put(actions.setLoadingState(true));

    const response = yield call(Api.fetchRecipes, {
      ...params,
      offset: currentOffset + AMOUNT_PER_FETCH,
      number: AMOUNT_PER_FETCH,
    });
    const recipesAttributes: RecipesAttributes = yield call(
      formatRecipesResponse,
      response
    );

    yield put(actions.updateRecipes(recipesAttributes));
  } catch (error) {
    yield put(
      globalActions.addMessage({
        type: MessageType.Error,
        text: error.message,
      })
    );
  } finally {
    yield put(actions.setLoadingState(false));
  }
}

export function* watchInitialRecipesFetch() {
  yield takeLatest(
    sagaActions.fetchInitialRecipes.toString(),
    fetchInitialRecipes
  );
}

export function* watchMoreRecipesFetch() {
  yield takeLatest(sagaActions.fetchMoreRecipes.toString(), fetchMoreRecipes);
}

export default [watchInitialRecipesFetch(), watchMoreRecipesFetch()];
