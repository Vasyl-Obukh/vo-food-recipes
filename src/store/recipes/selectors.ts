import { RootReducer } from 'store/types';

import { Slice, Recipe } from './types';

export const selectSlice = (state: RootReducer): Slice => state.recipes || {};
export const selectRecipes = (state: RootReducer): Recipe[] =>
  selectSlice(state).results || [];
export const selectRecipesLength = (state: RootReducer): number =>
  selectRecipes(state).length;
export const selectRecipesOffset = (state: RootReducer): number =>
  selectSlice(state).offset;
export const selectRecipesTotalResults = (state: RootReducer): number =>
  selectSlice(state).totalResults;
export const selectRecipesLoadingState = (state: RootReducer): boolean =>
  selectSlice(state).loading;
