import { createAction } from '@reduxjs/toolkit';

import { createStoreActionType, withPayloadType } from 'store/utils/actionType';
import { Params } from 'types/params';

const createRecipesActionType = createStoreActionType('recipes');

export const fetchInitialRecipes = createAction(
  createRecipesActionType('recipesInitialFetch'),
  withPayloadType<Params>()
);

export const fetchMoreRecipes = createAction(
  createRecipesActionType('moreRecipesFetch'),
  withPayloadType<Params>()
);
