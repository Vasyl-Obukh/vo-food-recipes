import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { Slice, RecipesAttributes } from './types';
import * as sagaActions from './actions';

const initialState: Slice = {
  results: [],
  offset: 0,
  totalResults: 0,
  loading: false,
};

const recipesSlice = createSlice({
  name: 'recipes',
  initialState,
  reducers: {
    updateRecipes(state, { payload }: PayloadAction<RecipesAttributes>) {
      return {
        ...state,
        ...payload,
        results: [...state.results, ...payload.results],
      };
    },
    clearRecipes() {
      return initialState;
    },
    setLoadingState(state, action: PayloadAction<boolean>) {
      state.loading = action.payload;
    },
  },
});

export const {
  fetchInitialRecipes,
  fetchMoreRecipes,
  updateRecipes,
  clearRecipes,
  setLoadingState,
} = { ...recipesSlice.actions, ...sagaActions };

export default recipesSlice.reducer;
