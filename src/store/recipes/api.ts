import { HttpClient } from 'utils/httpClient';
import { API_ROOT } from 'configs/apiRoot';
import { Params } from 'types/params';

export const fetchRecipes = async (params: Params) =>
  HttpClient.get(`${API_ROOT}/complexSearch`, {
    params,
  });
