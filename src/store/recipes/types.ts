import { Cuisine } from 'types/cuisine';
import { Diet } from 'types/diet';

export interface Recipe {
  id: number;
  title: string;
  image: string;
}

export interface RecipesAttributes {
  results: Recipe[];
  offset: number;
  totalResults: number;
}

export interface Slice extends RecipesAttributes {
  loading: boolean;
}

export interface FiltersFormData {
  diet: Diet[];
  cuisine: Cuisine[];
}
