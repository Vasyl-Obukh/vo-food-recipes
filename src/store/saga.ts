import { all } from 'redux-saga/effects';

import globalSaga from './global/saga';
import recipesSaga from './recipes/saga';
import recipeSaga from './recipe/saga';

export default function* saga() {
  yield all([...globalSaga, ...recipesSaga, ...recipeSaga]);
}
