import { combineReducers } from '@reduxjs/toolkit';

import globalReducer from './global';
import recipesReducer from './recipes';
import recipeReducer from './recipe';

export default combineReducers({
  global: globalReducer,
  recipes: recipesReducer,
  recipe: recipeReducer,
});
