export interface StringifiedParams {
  [key: string]: string | number;
}

export interface Params {
  [key: string]: string[] | number;
}
