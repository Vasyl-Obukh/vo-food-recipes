export type Cuisine =
  | 'american'
  | 'chinese'
  | 'french'
  | 'indian'
  | 'italian'
  | 'japanese';
