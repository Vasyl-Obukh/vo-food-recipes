import { RefObject, useEffect, useRef } from 'react';

export interface Disconnect {
  (): void;
}

export const useIntersectionObserver = <T extends Element>(
  callback: (disconnect: Disconnect) => void,
  options: IntersectionObserverInit | undefined
): RefObject<T> => {
  const target = useRef<T>(null);
  const observerRef = useRef<IntersectionObserver>();

  const intersectionHandler = (entries: IntersectionObserverEntry[]) => {
    const [targetEntry] = entries;

    if (targetEntry.isIntersecting) {
      callback(() => observerRef.current?.disconnect());
    }
  };

  useEffect(() => {
    observerRef.current = new IntersectionObserver(
      intersectionHandler,
      options
    );

    if (target.current) {
      observerRef.current.observe(target.current);
    }
  }, []);

  return target;
};
