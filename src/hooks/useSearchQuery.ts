import { useMemo } from 'react';
import { useLocation } from 'react-router-dom';

import { parseSearchStringToFilterParams } from 'utils/searchQuery';
import { Params } from 'types/params';

export const useSearchQuery = (keys: string[]): Params => {
  const { search } = useLocation();

  return useMemo(() => parseSearchStringToFilterParams(search, keys), [search]);
};
