import { RefObject, useEffect } from 'react';

export const useOutsideClick = (
  node: RefObject<HTMLDivElement>,
  handleClose: () => void
) => {
  const handleClickOutside = (event: MouseEvent) => {
    if (!node.current?.contains(event.target as Node)) {
      handleClose();
    }
  };

  useEffect(() => {
    document.addEventListener('mousedown', handleClickOutside);

    return () => document.removeEventListener('mousedown', handleClickOutside);
  }, []);
};
