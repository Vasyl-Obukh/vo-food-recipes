import { FiltersFormData } from 'store/recipes/types';
import { Params } from 'types/params';

export const createSearchStringFromFiltersFormData = (
  values: FiltersFormData
): string => {
  const urlSearchParams = new URLSearchParams();
  Object.entries(values).forEach(([key, value]) => {
    if (value.length) {
      urlSearchParams.set(key, value.toString());
    }
  });

  return urlSearchParams.toString();
};

export const parseSearchStringToFilterParams = (
  searchString: string,
  keys: string[]
): Params => {
  const urlSearchParams = new URLSearchParams(searchString);
  return keys.reduce((acc, key) => {
    const value = urlSearchParams.get(key);
    if (value) {
      return {
        ...acc,
        [key]: value.split(','),
      };
    }
    return acc;
  }, {});
};
