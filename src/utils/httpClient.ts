import axios, { AxiosRequestConfig } from 'axios';

import { Params, StringifiedParams } from 'types/params';

const stringifyParams = (params: Params | undefined): StringifiedParams =>
  params
    ? Object.fromEntries(
      Object.entries(params).map(([key, value]) => [key, value.toString()])
    )
    : {};

axios.interceptors.request.use(
  (config) => {
    const stringifiedParams: StringifiedParams = stringifyParams(config.params);
    config.params = {
      ...stringifiedParams,
      apiKey: process.env.REACT_APP_API_SECRET,
    };

    return config;
  },
  (error) => Promise.reject(error)
);

axios.interceptors.response.use(
  (response) => response.data,
  (error) => Promise.reject(error.response.data)
);

export const HttpClient = {
  get<T>(url: string, options?: AxiosRequestConfig): Promise<T> {
    return axios.get(url, options);
  },
  post<T>(url: string, data: object): Promise<T> {
    return axios.post(url, data);
  },
};
