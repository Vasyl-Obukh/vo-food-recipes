import { css, FlattenSimpleInterpolation } from 'styled-components';

type FontType = 'Regular' | 'Bold' | 'Italic' | 'Bold Italic';

export const includeFont = (type: FontType): FlattenSimpleInterpolation => css`
  font-family: 'Century Gothic ${type}', Arial, sans-serif;
`;
