enum Color {
  primary = '#531987',
  'primary-dim' = '#9881ad',
  error = '#dd0000',
  success = '#00dd00',
  info = '#dddd00',
  black = '#000000',
  white = '#ffffff',
  'gray-dark' = '#878787',
}

export type ColorName = keyof typeof Color;

export function getColor(color: ColorName): Color {
  return Color[color];
}
