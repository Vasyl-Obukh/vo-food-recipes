interface TextLabels {
  [key: string]: string;
}

export const textLabels: TextLabels = {
  'copyright-line1': 'Copyright © 2021 by Vasyl Obukh',
  'copyright-line2': 'All rights reserved',
  home: 'Food Recipes',
  'cuisine-checkbox-american': 'american',
  'cuisine-checkbox-chinese': 'chinese',
  'cuisine-checkbox-italian': 'italian',
  'cuisine-checkbox-french': 'french',
  'cuisine-checkbox-indian': 'indian',
  'cuisine-checkbox-japanese': 'japanese',
  'diet-checkbox-vegetarian': 'vegetarian',
  'diet-checkbox-vegan': 'vegan',
  'diet-checkbox-gluten free': 'gluten free',
  'filter-form-submit': 'Apply changes',
  'filter-form-title': 'Filter by',
  'filter-form-cuisine-group-title': 'Cuisines',
  'filter-form-diet-group-title': 'Diets',
  'extended-recipe-property-key-vegetarian': 'Vegetarian',
  'extended-recipe-property-key-vegan': 'Vegan',
  'extended-recipe-property-key-gluten-free': 'Gluten free',
  'extended-recipe-property-key-cheap': 'Cheap',
  'extended-recipe-property-key-ready-in-minutes': 'Ready in minutes',
  'extended-recipe-property-key-cuisines': 'Cuisines',
  'extended-recipe-property-key-diets': 'Diets',
  'easter-egg':
    'You scrolled through so many recipes, you did a great job, young man(lady) 😁😁😁',
};
