import styled from 'styled-components';

import { getColor } from 'styles/colors';

export const DefaultCheckbox = styled.input`
  display: none;
`;

export const Label = styled.label`
  display: flex;
  align-items: center;
  margin-top: 10px;
  font-size: 20px;
  cursor: pointer;
`;

export const Checkmark = styled.div`
  position: relative;
  width: 25px;
  height: 25px;
  margin-right: 15px;
  border: 3px solid ${getColor('gray-dark')};

  input:checked ~ &::after {
    position: absolute;
    top: -12px;
    left: 5px;
    width: 15px;
    height: 30px;
    border: solid;
    border-width: 0 5px 5px 0;
    content: '';
    transform: rotate(45deg);
  }
`;
