import React from 'react';
import { useField } from 'formik';

import * as Styled from './styles';

interface CheckboxProps {
  name: string;
  value: string;
}

export const Checkbox: React.FC<CheckboxProps> = ({ children, ...props }) => {
  const [field] = useField({ ...props, type: 'checkbox' });

  return (
    <Styled.Label>
      <Styled.DefaultCheckbox type="checkbox" {...field} {...props} />
      <Styled.Checkmark />
      <span>{children}</span>
    </Styled.Label>
  );
};
