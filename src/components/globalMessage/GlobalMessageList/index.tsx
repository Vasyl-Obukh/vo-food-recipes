import React from 'react';
import { useSelector } from 'react-redux';

import { selectMessages } from 'store/global/selectors';
import { Message } from 'store/global/types';
import { GlobalMessage } from 'components/globalMessage/GlobalMessage';

import * as Styled from './styles';

export const GlobalMessageList: React.FC<{}> = () => {
  const messages: Message[] = useSelector(selectMessages);

  return (
    <Styled.Wrapper>
      {messages.map((message) => (
        <GlobalMessage key={message.id} message={message} />
      ))}
    </Styled.Wrapper>
  );
};
