import styled from 'styled-components';

export const Wrapper = styled.section`
  position: absolute;
  z-index: 2;
  top: 105px;
  right: 0;
  left: 0;
  max-width: 620px;
  margin: 0 auto;
`;
