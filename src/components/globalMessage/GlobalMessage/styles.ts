import styled, { keyframes } from 'styled-components';

import { CloseButton } from 'components/styled/CloseButton';
import { getColor } from 'styles/colors';

const progressLine = keyframes`
  from {
    width: 0;
  }
  to {
    width: 100%;
  }
`;

export const Wrapper = styled.div`
  position: relative;
  z-index: 1;
  min-height: 70px;
  box-sizing: border-box;
  padding: 15px 0;
  border: 2px solid;
  margin-bottom: 20px;
  background-color: ${getColor('white')};
  font-size: 18px;
  opacity: 0.9;

  &.error {
    border-color: ${getColor('error')};
  }

  &.success {
    border-color: ${getColor('success')};
  }

  &.info {
    border-color: ${getColor('info')};
  }

  &::after {
    position: absolute;
    bottom: 0;
    display: block;
    width: 100%;
    height: 5px;
    animation: ${progressLine} 3s linear;
    background-color: ${getColor('primary')};
    content: '';
  }
`;

export const Content = styled.div`
  padding: 0 40px 0 25px;
`;

export const Type = styled.span`
  padding-right: 5px;
  text-transform: capitalize;

  &::after {
    content: ':';
  }

  .error & {
    color: ${getColor('error')};
  }

  .success & {
    color: ${getColor('success')};
  }

  .info & {
    color: ${getColor('info')};
  }
`;

export const CloseMessageButton = styled(CloseButton)`
  &::before,
  &::after {
    .error > & {
      border-color: ${getColor('error')};
    }

    .success > & {
      border-color: ${getColor('success')};
    }

    .info > & {
      border-color: ${getColor('info')};
    }
  }
`;
