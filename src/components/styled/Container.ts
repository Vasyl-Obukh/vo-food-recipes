import styled from 'styled-components';

import { MAX_WIDTH } from 'styles/grid';

export const Container = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  max-width: ${MAX_WIDTH};
  width: 100%;
  margin: 0 auto;
`;
