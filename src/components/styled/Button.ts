import styled from 'styled-components';

import { getColor } from 'styles/colors';

export const Button = styled.button`
  display: flex;
  width: 100%;
  min-width: 200px;
  min-height: 40px;
  box-sizing: border-box;
  align-items: center;
  justify-content: center;
  border: 2px solid transparent;
  margin-top: 10px;
  background-color: ${getColor('primary')};
  color: ${getColor('white')};
  cursor: pointer;
  font-size: 20px;
  transition: background-color, color 0.5s;

  &:hover {
    border-color: ${getColor('primary')};
    background-color: ${getColor('white')};
    color: ${getColor('primary')};
  }
`;
