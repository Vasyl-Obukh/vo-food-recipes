import styled from 'styled-components';

import { getColor } from 'styles/colors';

export const CloseButton = styled.div`
  position: absolute;
  top: 10px;
  right: 10px;
  width: 20px;
  height: 20px;
  cursor: pointer;

  &:hover {
    &::before,
    &::after {
      opacity: 0.5;
    }
  }

  &::before,
  &::after {
    position: absolute;
    top: calc(50% - 1px);
    left: -3px;
    width: 24px;
    border: 1px solid ${getColor('black')};
    content: '';
    transition: 0.25s;
  }

  &::before {
    transform: rotate(-45deg);
  }

  &::after {
    transform: rotate(45deg);
  }
`;
