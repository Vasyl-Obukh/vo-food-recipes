import styled from 'styled-components';

export const Main = styled.main`
  flex-grow: 1;
  padding: 30px 0 50px;
`;
