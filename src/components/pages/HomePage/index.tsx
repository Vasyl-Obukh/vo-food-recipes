import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';

import { PageContainer } from 'components/pageContainer/PageContainer';
import { Container } from 'components/styled/Container';
import { Row } from 'components/styled/Row';
import { RecipeTiles } from 'components/recipes/RecipeTiles';
import { RecipePopup } from 'components/recipes/RecipePopup';
import { Filters } from 'components/recipes/Filters';
import { RecipesInfiniteLooper } from 'components/recipes/RecipesInfiniteLooper';
import { ScrollToTop } from 'components/utils/ScrollToTop';
import * as recipesActions from 'store/recipes';
import { useSearchQuery } from 'hooks/useSearchQuery';
import { Params } from 'types/params';

import * as Styled from './styles';

export const HomePage: React.FC<{}> = () => {
  const dispatch = useDispatch();
  const filterParams: Params = useSearchQuery(['diet', 'cuisine']);

  useEffect(() => {
    dispatch(recipesActions.fetchInitialRecipes(filterParams));
  }, [dispatch, filterParams]);

  return (
    <PageContainer>
      <Styled.Main>
        <Container>
          <Row>
            <RecipeTiles />
            <Filters initialValues={filterParams} />
          </Row>
          <RecipePopup />
          <ScrollToTop />
          <RecipesInfiniteLooper filterParams={filterParams} />
        </Container>
      </Styled.Main>
    </PageContainer>
  );
};
