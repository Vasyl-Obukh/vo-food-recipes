import React from 'react';

import { getColor } from 'styles/colors';

import * as Styled from './styles';

const WIDTH = 100;
const HEIGHT = 100;
const RADIUS = 50;

export const Spinner: React.FC<{}> = () => {
  return (
    <Styled.Container>
      <svg
        width={WIDTH}
        height={HEIGHT}
        viewBox="0 0 38 38"
        xmlns="http://www.w3.org/2000/svg"
        stroke={getColor('primary')}
      >
        <g fill="none" fillRule="evenodd">
          <g transform="translate(1 1)" strokeWidth="2">
            <circle strokeOpacity=".5" cx="18" cy="18" r={RADIUS} />
            <path d="M36 18c0-9.94-8.06-18-18-18">
              <animateTransform
                attributeName="transform"
                type="rotate"
                from="0 18 18"
                to="360 18 18"
                dur="1s"
                repeatCount="indefinite"
              />
            </path>
          </g>
        </g>
      </svg>
    </Styled.Container>
  );
};
