import React, { useState } from 'react';

import {
  Disconnect,
  useIntersectionObserver,
} from 'hooks/useIntersectionObserver';

import * as Styled from './styles';

interface ImageLazyLoadProps {
  src: string;
  alt: string;
}

const preloadImage =
  'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkqAcAAIUAgUW0RjgAAAAASUVORK5CYII=';

export const ImageLazyLoad: React.FC<ImageLazyLoadProps> = ({ src, alt }) => {
  const [source, setSource] = useState(preloadImage);

  const handler = (disconnect: Disconnect) => {
    setSource(src);
    disconnect();
  };

  const ref = useIntersectionObserver<HTMLImageElement>(handler, {
    rootMargin: '200px',
  });

  return <Styled.Image ref={ref} alt={alt} src={source} />;
};
