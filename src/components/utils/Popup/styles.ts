import styled from 'styled-components';
import { transparentize } from 'polished';

import { getColor } from 'styles/colors';

export const Background = styled.div`
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 3;
  display: flex;
  align-items: flex-start;
  justify-content: center;
  padding-top: 120px;
  background-color: ${transparentize(0.67, getColor('black'))};
  cursor: pointer;
`;

export const Container = styled.div`
  position: relative;
  box-sizing: border-box;
  flex: 0 1 800px;
  padding: 30px 40px;
  min-height: 300px;
  max-height: 90vh;
  background-color: ${getColor('white')};
  cursor: auto;
`;
