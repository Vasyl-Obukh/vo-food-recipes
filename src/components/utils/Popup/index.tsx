import React, { useEffect, useRef } from 'react';

import { CloseButton } from 'components/styled/CloseButton';
import { useOutsideClick } from 'hooks/useOutsideClick';

import * as Styled from './styles';

interface PopupProps {
  handleClose: () => void;
}

export const Popup: React.FC<PopupProps> = ({ children, handleClose }) => {
  const node = useRef<HTMLDivElement>(null);

  useEffect(() => {
    document.body.style.overflow = 'hidden';

    return () => {
      document.body.style.overflow = 'unset';
    };
  }, []);

  useOutsideClick(node, handleClose);

  return (
    <Styled.Background>
      <Styled.Container ref={node}>
        <CloseButton onClick={handleClose} />
        {children}
      </Styled.Container>
    </Styled.Background>
  );
};
