import React, { useEffect, useState } from 'react';

import * as Styled from './styles';

export const ScrollToTop: React.FC<{}> = () => {
  const [isVisible, setIsVisible] = useState(false);
  const distance = 500;

  const toggleVisibility = () => {
    setIsVisible(window.pageYOffset > distance);
  };

  useEffect(() => {
    window.addEventListener('scroll', toggleVisibility);

    return () => window.removeEventListener('scroll', toggleVisibility);
  }, []);

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  };

  return <Styled.Button visible={isVisible} onClick={scrollToTop} />;
};
