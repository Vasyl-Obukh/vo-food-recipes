import styled from 'styled-components';

import { getColor } from 'styles/colors';

export interface ButtonProps {
  visible: boolean;
}

export const Button = styled.div<ButtonProps>`
  position: fixed;
  display: inline-block;
  right: ${({ visible }) => (visible ? '50px' : '-100px')};
  bottom: 150px;
  padding: 15px;
  border: solid ${getColor('primary-dim')};
  border-width: 0 5px 5px 0;
  transform: rotate(-135deg);
  transition: right 0.5s, border-color 0.25s;
  cursor: pointer;

  &:hover {
    border-color: ${getColor('primary')};
  }
`;
