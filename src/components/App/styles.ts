import styled from 'styled-components';

import { includeFont } from 'styles/fonts';

export const Wrapper = styled.div`
  display: flex;
  min-height: 100vh;
  flex-direction: column;
  ${includeFont('Regular')};
  overflow: hidden;
`;
