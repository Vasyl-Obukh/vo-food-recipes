import React from 'react';

import { HomePage } from 'components/pages/HomePage';

import * as Styled from './styles';

export function App() {
  return (
    <Styled.Wrapper>
      <HomePage />
    </Styled.Wrapper>
  );
}
