import React from 'react';

import { Header } from 'components/pageContainer/Header';
import { Footer } from 'components/pageContainer/Footer';
import { GlobalMessageList } from 'components/globalMessage/GlobalMessageList';

export const PageContainer: React.FC<{}> = ({ children }) => {
  return (
    <>
      <Header />
      <GlobalMessageList />
      {children}
      <Footer />
    </>
  );
};
