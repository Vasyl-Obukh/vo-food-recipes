import { Link } from 'react-router-dom';

import styled from 'styled-components';
import { getColor } from 'styles/colors';

export const Wrapper = styled.header`
  display: flex;
  height: 60px;
  background-color: ${getColor('primary')};
`;

export const HomeLink = styled(Link)`
  display: flex;
  align-items: center;
  font-size: 25px;
  text-decoration: none;
  color: ${getColor('white')};
  transition: opacity 0.25s;

  &:hover {
    opacity: 0.75;
  }
`;
