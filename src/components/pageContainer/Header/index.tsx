import React from 'react';

import { Container } from 'components/styled/Container';
import { Row } from 'components/styled/Row';
import { textLabels } from 'configs/textLabels';

import * as Styled from './styles';

export function Header() {
  return (
    <Styled.Wrapper>
      <Container>
        <Row>
          <Styled.HomeLink to="/">{textLabels.home}</Styled.HomeLink>
        </Row>
      </Container>
    </Styled.Wrapper>
  );
}
