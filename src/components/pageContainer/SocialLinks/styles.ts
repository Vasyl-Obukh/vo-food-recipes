import { FunctionComponent } from 'react';
import styled from 'styled-components';

import { getColor } from 'styles/colors';

export const List = styled.ul`
  display: flex;
  justify-content: center;
  padding: 0 0 15px;
  margin: 0;
  list-style: none;
`;

export const Item = styled.li`
  padding: 0 16px;
`;

export const Link = styled.a`
  color: ${getColor('white')};
  transition: color 0.5s;
  will-change: color;

  &:hover {
    color: ${getColor('black')};
  }
`;

export function getStyledLink(component: FunctionComponent) {
  return styled(component)`
    height: 40px;
  `;
}
