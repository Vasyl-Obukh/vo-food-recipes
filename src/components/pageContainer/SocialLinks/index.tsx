import React from 'react';

import * as Styled from './styles';
import { socialLinks } from './links';

export const SocialLinks: React.FC<{}> = () => {
  return (
    <Styled.List>
      {socialLinks.map((link) => (
        <Styled.Item key={link.name}>
          <Styled.Link href={link.url}>
            <link.component />
          </Styled.Link>
        </Styled.Item>
      ))}
    </Styled.List>
  );
};
