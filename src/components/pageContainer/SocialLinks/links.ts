import { FunctionComponent } from 'react';

import { ReactComponent as YouTube } from 'assets/icons/youtube.svg';
import { ReactComponent as Facebook } from 'assets/icons/facebook.svg';
import { ReactComponent as Twitter } from 'assets/icons/twitter.svg';
import { ReactComponent as Instagram } from 'assets/icons/instagram.svg';
import { ReactComponent as LinkedIn } from 'assets/icons/linkedin.svg';

import { getStyledLink } from './styles';

export interface SocialLink {
  component: FunctionComponent;
  name: string;
  url: string;
}

export const socialLinks: SocialLink[] = [
  {
    component: getStyledLink(YouTube),
    name: 'youtube',
    url: 'https://www.youtube.com/channel/UCqD3lg-4bSVCKFhXBRJUFmg',
  },
  {
    component: getStyledLink(Facebook),
    name: 'facebook',
    url: 'https://www.facebook.com/vasia.obyh',
  },
  {
    component: getStyledLink(Twitter),
    name: 'twitter',
    url: 'https://twitter.com/ObukhVasia',
  },
  {
    component: getStyledLink(Instagram),
    name: 'instagram',
    url: 'https://www.instagram.com/vasia_obukh',
  },
  {
    component: getStyledLink(LinkedIn),
    name: 'linkedIn',
    url: 'https://www.linkedin.com/in/vasyl-obukh-682ba7188/',
  },
];
