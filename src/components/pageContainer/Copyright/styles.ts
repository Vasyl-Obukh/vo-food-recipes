import styled from 'styled-components';

export const Wrapper = styled.div`
  padding-top: 15px;
  text-align: center;
`;

export const Paragraph = styled.p`
  margin: 0;
  font-size: 12px;
  line-height: 14px;
`;
