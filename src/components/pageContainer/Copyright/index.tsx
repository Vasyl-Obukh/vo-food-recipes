import React from 'react';

import { textLabels } from 'configs/textLabels';

import * as Styled from './styles';

export function Copyright() {
  return (
    <Styled.Wrapper>
      <Styled.Paragraph>{textLabels['copyright-line1']}</Styled.Paragraph>
      <Styled.Paragraph>{textLabels['copyright-line2']}</Styled.Paragraph>
    </Styled.Wrapper>
  );
}
