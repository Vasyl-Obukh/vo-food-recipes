import styled from 'styled-components';

import { includeFont } from 'styles/fonts';

export const Paragraph = styled.p`
  ${includeFont('Bold')};
  margin-top: 0;
  text-align: center;
  text-transform: uppercase;
`;
