import React from 'react';
import { useSelector } from 'react-redux';

import { selectRecipesLength } from 'store/recipes/selectors';
import { textLabels } from 'configs/textLabels';

import * as Styled from './styles';

export const EasterEgg: React.FC<{}> = () => {
  const recipesLength = useSelector(selectRecipesLength);

  return recipesLength > 100 ? (
    <Styled.Paragraph>{textLabels['easter-egg']}</Styled.Paragraph>
  ) : null;
};
