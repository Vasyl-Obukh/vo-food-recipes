import React from 'react';

import { Container } from 'components/styled/Container';
import { Row } from 'components/styled/Row';
import { SocialLinks } from 'components/pageContainer/SocialLinks';
import { EasterEgg } from 'components/pageContainer/EasterEgg';
import { Copyright } from 'components/pageContainer/Copyright';

import * as Styled from './styles';

export function Footer() {
  return (
    <Styled.Wrapper>
      <Container>
        <Row>
          <Styled.Content>
            <EasterEgg />
            <SocialLinks />
            <Copyright />
          </Styled.Content>
        </Row>
      </Container>
    </Styled.Wrapper>
  );
}
