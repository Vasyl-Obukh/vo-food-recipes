import styled from 'styled-components';

import { getColor } from 'styles/colors';

export const Wrapper = styled.footer`
  padding: 15px 0;
  background-color: ${getColor('primary')};
  color: ${getColor('white')};
`;

export const Content = styled.div`
  flex-basis: 100%;
`;
