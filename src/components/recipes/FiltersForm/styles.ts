import styled from 'styled-components';
import { Form as FormikForm } from 'formik';

import { includeFont } from 'styles/fonts';

export const Form = styled(FormikForm)`
  padding: 20px 0 20px 25px;
`;

export const Title = styled.div`
  ${includeFont('Bold')};
  font-size: 25px;
`;

export const GroupTitle = styled.div`
  padding-top: 15px;
  font-size: 22px;
`;

export const CheckboxGroup = styled.div`
  padding-left: 15px;
`;
