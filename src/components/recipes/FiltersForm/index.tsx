import React from 'react';
import { Formik } from 'formik';

import { Checkbox } from 'components/formElements/Checkbox';
import { Button } from 'components/styled/Button';
import { FiltersFormData } from 'store/recipes/types';
import { textLabels } from 'configs/textLabels';
import { Cuisine } from 'types/cuisine';
import { Diet } from 'types/diet';

import * as Styled from './styles';

interface FiltersFormProps {
  onSubmit: (values: FiltersFormData) => void;
  initialValues: FiltersFormData;
}

const cuisines: Cuisine[] = [
  'american',
  'chinese',
  'italian',
  'french',
  'indian',
  'japanese',
];

const diets: Diet[] = ['vegetarian', 'vegan', 'gluten free'];

export const FiltersForm: React.FC<FiltersFormProps> = ({
  onSubmit,
  initialValues,
}) => {
  return (
    <Formik
      initialValues={initialValues}
      onSubmit={onSubmit}
      enableReinitialize
    >
      <Styled.Form>
        <Styled.Title>{textLabels['filter-form-title']}</Styled.Title>
        <Styled.GroupTitle>
          {textLabels['filter-form-cuisine-group-title']}
        </Styled.GroupTitle>
        <Styled.CheckboxGroup>
          {cuisines.map((cuisine) => (
            <Checkbox key={cuisine} name="cuisine" value={cuisine}>
              {textLabels[`cuisine-checkbox-${cuisine}`]}
            </Checkbox>
          ))}
        </Styled.CheckboxGroup>
        <Styled.GroupTitle>
          {textLabels['filter-form-diet-group-title']}
        </Styled.GroupTitle>
        <Styled.CheckboxGroup>
          {diets.map((diet) => (
            <Checkbox key={diet} name="diet" value={diet}>
              {textLabels[`diet-checkbox-${diet}`]}
            </Checkbox>
          ))}
        </Styled.CheckboxGroup>
        <Button type="submit">{textLabels['filter-form-submit']}</Button>
      </Styled.Form>
    </Formik>
  );
};
