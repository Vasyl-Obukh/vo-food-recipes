import React, { useCallback } from 'react';
import { useHistory } from 'react-router-dom';

import { FiltersForm } from 'components/recipes/FiltersForm';
import { createSearchStringFromFiltersFormData } from 'utils/searchQuery';
import { FiltersFormData } from 'store/recipes/types';
import { Params } from 'types/params';

interface FiltersProps {
  initialValues: Params;
}

export const Filters: React.FC<FiltersProps> = ({ initialValues }) => {
  const history = useHistory();

  const filterResults = useCallback(
    (values: FiltersFormData): void => {
      history.push({
        search: createSearchStringFromFiltersFormData(values),
      });
    },
    [history]
  );

  return (
    <FiltersForm
      initialValues={(initialValues as unknown) as FiltersFormData}
      onSubmit={filterResults}
    />
  );
};
