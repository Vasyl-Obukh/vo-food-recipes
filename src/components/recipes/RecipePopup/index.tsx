import React, { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { Popup } from 'components/utils/Popup';
import { Recipe } from 'components/recipes/Recipe';
import { Spinner } from 'components/utils/Spinner';
import * as recipeActions from 'store/recipe';
import { selectRecipe, selectRecipeLoadingState } from 'store/recipe/selectors';

export const RecipePopup: React.FC<{}> = () => {
  const dispatch = useDispatch();
  const recipe = useSelector(selectRecipe);
  const isLoading = useSelector(selectRecipeLoadingState);

  const clearRecipe = useCallback(
    () => dispatch(recipeActions.clearRecipe()),
    []
  );

  return recipe || isLoading ? (
    <Popup handleClose={clearRecipe}>
      {recipe ? <Recipe recipe={recipe} /> : <Spinner />}
    </Popup>
  ) : null;
};
