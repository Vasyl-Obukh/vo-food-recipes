import styled from 'styled-components';

export const Container = styled.div`
  flex-grow: 1;
`;

export const Wrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-auto-rows: 250px;
  grid-gap: 15px;
  width: 100%;
  padding: 20px 0;
`;
