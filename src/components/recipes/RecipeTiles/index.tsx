import React from 'react';
import { useSelector } from 'react-redux';

import { RecipeTile } from 'components/recipes/RecipeTile';
import { Spinner } from 'components/utils/Spinner';
import {
  selectRecipes,
  selectRecipesLoadingState,
} from 'store/recipes/selectors';

import * as Styled from './styles';

export const RecipeTiles: React.FC<{}> = () => {
  const recipes = useSelector(selectRecipes);
  const isLoading = useSelector(selectRecipesLoadingState);

  return (
    <Styled.Container>
      <Styled.Wrapper>
        {recipes.map((recipe) => (
          <RecipeTile key={recipe.id} recipe={recipe} />
        ))}
      </Styled.Wrapper>
      {isLoading ? <Spinner /> : null}
    </Styled.Container>
  );
};
