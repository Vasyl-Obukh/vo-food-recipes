import React from 'react';
import { useDispatch } from 'react-redux';

import * as recipesActions from 'store/recipes';
import { useIntersectionObserver } from 'hooks/useIntersectionObserver';
import { Params } from 'types/params';

interface RecipesInfiniteLooperProps {
  filterParams: Params;
}

export const RecipesInfiniteLooper: React.FC<RecipesInfiniteLooperProps> = ({
  filterParams,
}) => {
  const dispatch = useDispatch();

  const loadMore = () =>
    dispatch(recipesActions.fetchMoreRecipes(filterParams));
  const ref = useIntersectionObserver<HTMLDivElement>(loadMore, {
    rootMargin: '200px',
  });

  return <div ref={ref} />;
};
