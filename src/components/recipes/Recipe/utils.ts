enum Answer {
  YES = 'Yes',
  NO = 'No',
}

export const transformToYesNo = (property: boolean): Answer =>
  property ? Answer.YES : Answer.NO;
