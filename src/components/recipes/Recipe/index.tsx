import React from 'react';

import { textLabels } from 'configs/textLabels';
import { ExtendedRecipe } from 'store/recipe/types';

import * as Styled from './styles';
import { transformToYesNo } from './utils';

interface RecipeTileProps {
  recipe: ExtendedRecipe;
}

export const Recipe: React.FC<RecipeTileProps> = ({
  recipe: {
    title,
    image,
    vegetarian,
    vegan,
    glutenFree,
    cheap,
    readyInMinutes,
    cuisines,
    summary,
    diets,
  },
}) => {
  return (
    <Styled.Container>
      <Styled.Image src={image} alt={title} />
      <Styled.Properties>
        <Styled.Title>{title}</Styled.Title>
        <div>
          <Styled.PropertyKey>
            {textLabels['extended-recipe-property-key-vegetarian']}
          </Styled.PropertyKey>
          <Styled.PropertyValue>
            {transformToYesNo(vegetarian)}
          </Styled.PropertyValue>
        </div>
        <div>
          <Styled.PropertyKey>
            {textLabels['extended-recipe-property-key-vegan']}
          </Styled.PropertyKey>
          <Styled.PropertyValue>{transformToYesNo(vegan)}</Styled.PropertyValue>
        </div>
        <div>
          <Styled.PropertyKey>
            {textLabels['extended-recipe-property-key-gluten-free']}
          </Styled.PropertyKey>
          <Styled.PropertyValue>
            {transformToYesNo(glutenFree)}
          </Styled.PropertyValue>
        </div>
        <div>
          <Styled.PropertyKey>
            {textLabels['extended-recipe-property-key-cheap']}
          </Styled.PropertyKey>
          <Styled.PropertyValue>{transformToYesNo(cheap)}</Styled.PropertyValue>
        </div>
        <div>
          <Styled.PropertyKey>
            {textLabels['extended-recipe-property-key-ready-in-minutes']}
          </Styled.PropertyKey>
          <Styled.PropertyValue>{readyInMinutes}</Styled.PropertyValue>
        </div>
        <div>
          <Styled.PropertyKey>
            {textLabels['extended-recipe-property-key-cuisines']}
          </Styled.PropertyKey>
          <Styled.PropertyValue>{cuisines.toString()}</Styled.PropertyValue>
        </div>
        <div>
          <Styled.PropertyKey>
            {textLabels['extended-recipe-property-key-diets']}
          </Styled.PropertyKey>
          <Styled.PropertyValue>{diets.toString()}</Styled.PropertyValue>
        </div>
      </Styled.Properties>
      <Styled.Summary dangerouslySetInnerHTML={{ __html: summary }} />
    </Styled.Container>
  );
};
