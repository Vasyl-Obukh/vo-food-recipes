import styled from 'styled-components';

import { getColor } from 'styles/colors';
import { includeFont } from 'styles/fonts';

export const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

export const Image = styled.img`
  width: 50%;
  height: 250px;
  object-fit: cover;
`;

export const Properties = styled.div`
  box-sizing: border-box;
  width: 50%;
  padding-left: 10px;
`;

export const Title = styled.div`
  ${includeFont('Bold')};
  padding-bottom: 5px;
  font-size: 20px;
  color: ${getColor('primary')};
`;

export const Summary = styled.div`
  ${includeFont('Italic')};
  padding-top: 10px;
  & > a {
    color: ${getColor('black')};
  }
`;

export const PropertyKey = styled.span`
  ${includeFont('Bold')};

  &::after {
    content: ': ';
  }
`;

export const PropertyValue = styled.span`
  ${includeFont('Italic')};
`;
