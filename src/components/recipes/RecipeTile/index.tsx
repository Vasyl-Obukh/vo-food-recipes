import React from 'react';
import { useDispatch } from 'react-redux';

import { ImageLazyLoad } from 'components/utils/ImageLazyLoad';
import { Recipe } from 'store/recipes/types';
import * as recipeActions from 'store/recipe';

import * as Styled from './styles';

interface RecipeTileProps {
  recipe: Recipe;
}

export const RecipeTile: React.FC<RecipeTileProps> = (props) => {
  const dispatch = useDispatch();
  const { title, image, id } = props.recipe;

  const fetchExtendedRecipe = () => dispatch(recipeActions.fetchRecipe(id));

  return (
    <Styled.Container onClick={fetchExtendedRecipe}>
      <ImageLazyLoad src={image} alt={title} />
      <Styled.Content>
        <Styled.Title>{title}</Styled.Title>
      </Styled.Content>
    </Styled.Container>
  );
};
