import styled from 'styled-components';
import { transparentize } from 'polished';

import { getColor } from 'styles/colors';
import { includeFont } from 'styles/fonts';

export const Content = styled.div`
  box-sizing: border-box;
  position: absolute;
  width: 100%;
  height: 100%;
  transition: background-color 0.25s;
`;

export const Title = styled.div`
  ${includeFont('Bold')};
  position: absolute;
  padding: 15px;
  font-size: 20px;
  color: ${getColor('white')};
  top: 100%;
  transition: all 0.25s;
`;

export const Container = styled.div`
  position: relative;
  display: flex;
  border: 2px solid ${getColor('gray-dark')};
  text-decoration: none;
  overflow: hidden;
  cursor: pointer;

  &:hover {
    & ${Content} {
      background-color: ${transparentize(0.67, getColor('black'))};
    }

    & ${Title} {
      transform: translate3d(0, -100%, 0);
    }
  }
`;
